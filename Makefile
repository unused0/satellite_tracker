all : tracker tracker_text tags

OPT = -O0 -g
FFLAGS = $(OPT) -fno-automatic -std=legacy 
CFLAGS = $(OPT)
SRCS = tracker.ftn plot10.ftn plot10a.ftn plot10b.ftn ade.c sgp4.ftn fmod2p.ftn actan.ftn driver.ftn map.ftn readtle.ftn blkdata.ftn
OBJS = tracker.o plot10.o plot10a.o plot10b.o ade.o sgp4.o fmod2p.o actan.o driver.o map.o readtle.o blkdata.o

tracker : $(OBJS)
	gfortran $(OBJS) -o tracker -g

tracker.o : tracker.ftn
	gfortran -c tracker.ftn $(FFLAGS) -fimplicit-none -Wall

readtle.o : readtle.ftn
	gfortran -c readtle.ftn $(FFLAGS) -fimplicit-none -Wall

map.o : map.ftn
	gfortran -c map.ftn $(FFLAGS) -fimplicit-none -Wall

plot10.o : plot10.ftn
	gfortran -c plot10.ftn $(FFLAGS)

plot10a.o : plot10a.ftn
	gfortran -c plot10a.ftn $(FFLAGS)

plot10b.o : plot10b.ftn
	gfortran -c plot10b.ftn $(FFLAGS)

ade.o : ade.c
	gcc -c ade.c $(CFLAGS)

sgp4.o : sgp4.ftn
	gfortran -c sgp4.ftn $(FFLAGS)

fmod2p.o : fmod2p.ftn
	gfortran -c fmod2p.ftn $(FFLAGS)

actan.o : actan.ftn
	gfortran -c actan.ftn $(FFLAGS)

driver.o : driver.ftn
	gfortran -c driver.ftn $(FFLAGS)

blkdata.o : blkdata.ftn
	gfortran -c blkdata.ftn $(FFLAGS)

blkdata.ftn : initmap map.data
	./initmap

initmap : initmap.ftn
	gfortran initmap.ftn -o initmap $(FFLAGS)

tracker_text: tracker_text.ftn driver.o readtle.o sgp4.o fmod2p.o actan.o curses.o
	gfortran tracker_text.ftn -o tracker_text $(FFLAGS) driver.o readtle.o sgp4.o fmod2p.o actan.o curses.o -lcurses

curses.o : curses.c
	gcc -c curses.c

clean : 
	-rm $(OBJS) tracker

tags : $(SRCS)
	ctags $(SRCS)
