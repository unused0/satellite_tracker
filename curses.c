#include <curses.h>
#include <string.h>


void winit_ (void)
  {
    initscr();
  }

void wtext_ (int * r, int * c, char * text, int * l)
  {
    char s [* l + 1];
    strncpy (s, text, * l);
    s [* l] = 0;
    mvprintw (* r, * c, text);
  }

void wsync_ (void)
  {
    refresh ();
  }

